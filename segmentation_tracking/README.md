# To do....

Segmentation and tracking was using an in-house implementation of a graphical user interface calling a watershed segmentation algorithm
available at https://gitlab.inria.fr/mosaic/timagetk, followed by manual corrections.

All segmentation and tracking output data used in the paper is available together with the confocal data at the data repository 
https://doi.org/10.17863/CAM.7793 .
